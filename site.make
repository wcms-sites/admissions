core = 7.x
api = 2

/** Custom Modules
 *  Alphabetically Ordered
 */

; uw_program_search
projects[uw_program_search][type] = "module"
projects[uw_program_search][download][type] = "git"
projects[uw_program_search][download][url] = "https://git.uwaterloo.ca/wcms/uw_program_search.git"
projects[uw_program_search][download][tag] = "7.x-2.0"
projects[uw_program_search][subdir] = "features"


 /** Feature modules
 * Alphabetically ordered
 */
 
; uw_cfg_admissions_site
projects[uw_cfg_admissions_site][type] = "module"
projects[uw_cfg_admissions_site][download][type] = "git"
projects[uw_cfg_admissions_site][download][url] = "https://git.uwaterloo.ca/wcms/uw_cfg_admissions_site.git"
projects[uw_cfg_admissions_site][download][tag] = "7.x-1.0"
projects[uw_cfg_admissions_site][subdir] = "features"

